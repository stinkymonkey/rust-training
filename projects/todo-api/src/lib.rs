mod database;
mod dtos;
mod handlers;
mod middlewares;
mod routes;
mod utils;

use dotenvy_macro::dotenv;
use routes::create_routes;
use sea_orm::Database;

const SERVER_ADDRESS_PORT: &'static str = "127.0.0.1:3000";
pub const API_VERSION: &'static str = env!("CARGO_PKG_VERSION");
pub const ENVIRONMENT: &'static str = dotenv!("ENVIRONMENT");
pub const JWT_SECRET: &'static str = dotenv!("JWT_SECRET");

pub const AUTH_TOKEN: &'static str = "auth_token";

pub async fn run_app(database_uri: &str) {
    let database = Database::connect(database_uri).await.unwrap();
    let app = create_routes(database);

    println!("API SERVER listening on {:?}", &SERVER_ADDRESS_PORT);
    axum::Server::bind(&SERVER_ADDRESS_PORT.parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap()
}
