use argon2::{Argon2, PasswordHash, PasswordHasher, PasswordVerifier};
use argon2::password_hash::SaltString;
use axum::Extension;
use axum::Json;
use jsonwebtoken::{encode, EncodingKey, Header};
use rand_core::OsRng;
use sea_orm::DatabaseConnection;
use sea_orm::{ActiveValue::Set, ActiveModelTrait, ColumnTrait, EntityTrait, QueryFilter};
use crate::database::prelude::Users;
use crate::database::users;
use crate::database::users::Model;
use crate::dtos::user_dtos::{CreateUserRequestDto, LoginUserRequestDto, TokenClaims};
use crate::handlers::response_generator::{ErrorResponse, ResponseMessage, HandlerResult, SuccessResponse};
use tower_cookies::{Cookie, Cookies};
use tower_cookies::cookie::{SameSite, time};
use crate::{AUTH_TOKEN, JWT_SECRET};

pub async fn create_user(
    Extension(database): Extension<DatabaseConnection>,
    Json(create_user_request_dto): Json<CreateUserRequestDto>
) -> HandlerResult<Model> {
    let user = Users::find().filter(
        users::Column::Username.eq(&create_user_request_dto.username)
    ).one(&database)
    .await
    .map_err(|e| {
        ErrorResponse::UnhandledException(e.to_string())
    })?;

    if let Some(_exists) = user {
        return Err(ErrorResponse::FailureToCreateResource("username is not available, please try again with different username".to_owned()))
    }

    let salt = SaltString::generate(&mut OsRng);

    let hashed_password = Argon2::default()
        .hash_password(create_user_request_dto.password.as_bytes(), &salt)
        .map_err(|e| {
            ErrorResponse::UnhandledException(e.to_string())
        })
        .map(|hash| hash.to_string())?;

    let user = users::ActiveModel {
        username: Set(create_user_request_dto.username),
        password: Set(hashed_password),
        ..Default::default()
    };

    let user = user.insert(&database).await;

    if let Err(e) = &user {
        return Err(ErrorResponse::FailureToCreateResource(e.to_string()));
    }

    let body = SuccessResponse::new(
        Some(user.unwrap()),
        ResponseMessage::ResourceCreateSuccess
    );

    Ok(Json(body))
}

pub async fn login_user(
    cookies: Cookies,
    Extension(database): Extension<DatabaseConnection>,
    Json(login_user_request): Json<LoginUserRequestDto>
) -> HandlerResult<String> {
    let user = Users::find().filter(
        users::Column::Username.eq(&login_user_request.username)
    ).one(&database)
    .await
    .map_err(|e| {
        ErrorResponse::UnhandledException(e.to_string())
    })?
    .ok_or_else(|| {
        ErrorResponse::AuthenticationFailed
    })?;

    let is_password_match: bool = match PasswordHash::new(&user.password) {
        Ok(parsed_hash) => Argon2::default()
            .verify_password(login_user_request.password.as_bytes(), &parsed_hash)
            .map_or(false, |_| true),
        Err(_) => false,
    };

    if !is_password_match {
        return Err(ErrorResponse::AuthenticationFailed);
    }

    let now = chrono::Utc::now();
    let iat = now.timestamp() as usize;
    let exp = (now + chrono::Duration::minutes(60)).timestamp() as usize;
    let claims: TokenClaims = TokenClaims {
        sub: user.id.to_string(),
        exp,
        iat,
    };

    let token = encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(JWT_SECRET.as_ref()),
    ).unwrap();

    cookies.add(
        Cookie::build(AUTH_TOKEN, token.to_owned())
            .path("/")
            .max_age(time::Duration::hours(1))
            .same_site(SameSite::Lax)
            .http_only(true)
            .finish()
    );

    let body = SuccessResponse::new(
        None,
        ResponseMessage::AuthenticationSuccess
    );

    Ok(Json(body))
}
