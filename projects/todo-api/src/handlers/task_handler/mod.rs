use crate::database::prelude::Tasks;
use crate::database::tasks;
use crate::database::tasks::{Entity, Model};
use crate::dtos::{
    task_dtos::{CreateTaskRequestDto, ListTasksRequestParametersDto, UpdateTaskRequestDto},
    DeleteResultDto,
};
use crate::handlers::response_generator::{
    ErrorResponse, HandlerResult,
    ResponseMessage::{
        ResourceCreateSuccess, ResourceDeleteSuccess, ResourceFetchDetailsSuccess,
        ResourceFetchListSuccess, ResourceUpdateSuccess,
    },
    SuccessResponse,
};
use crate::utils::create_like_filter;
use axum::extract::{Path, Query};
use axum::response::IntoResponse;
use axum::{Extension, Json};
use sea_orm::sea_query::IntoColumnRef;
use sea_orm::ActiveValue::Set;
use sea_orm::{ActiveModelTrait, DatabaseConnection, EntityTrait, ModelTrait, QueryFilter, Select};

pub async fn create_task(
    Extension(database): Extension<DatabaseConnection>,
    Json(create_task_request_dto): Json<CreateTaskRequestDto>,
) -> impl IntoResponse {
    let new_task = tasks::ActiveModel {
        priority: Set(create_task_request_dto.priority),
        title: Set(create_task_request_dto.title),
        description: Set(create_task_request_dto.description),
        ..Default::default()
    };

    let task = new_task.insert(&database).await;

    if let Err(_e) = &task {
        return Err(ErrorResponse::ResourceCreateFailed);
    }

    let body = SuccessResponse::new(Some(task.unwrap()), ResourceCreateSuccess);

    Ok(Json(body))
}

pub async fn get_all_tasks(
    Extension(database): Extension<DatabaseConnection>,
    Query(list_task_request_parameters): Query<ListTasksRequestParametersDto>,
) -> HandlerResult<Vec<Model>> {
    let mut tasks: Select<Entity> = Tasks::find();

    if let Some(title) = list_task_request_parameters.title {
        tasks = tasks.filter(create_like_filter(
            tasks::Column::Title.into_column_ref(),
            title,
        ));
    }

    if let Some(priority) = list_task_request_parameters.priority {
        tasks = tasks.filter(create_like_filter(
            tasks::Column::Priority.into_column_ref(),
            priority,
        ));
    }

    if let Some(description) = list_task_request_parameters.description {
        tasks = tasks.filter(create_like_filter(
            tasks::Column::Description.into_column_ref(),
            description,
        ));
    }

    let tasks = tasks.all(&database).await;

    if let Err(_e) = &tasks {
        return Err(ErrorResponse::ResourceFetchListFailed);
    }

    let body = SuccessResponse::new(Some(tasks.unwrap()), ResourceFetchListSuccess);

    Ok(Json(body))
}

pub async fn get_task_by_id(
    Extension(database): Extension<DatabaseConnection>,
    Path(task_id): Path<i32>,
) -> HandlerResult<Option<Model>> {
    let task = Tasks::find_by_id(task_id).one(&database).await.unwrap();

    if let None = &task {
        return Err(ErrorResponse::ResourceNotFound);
    }

    let body = SuccessResponse::new(Some(task), ResourceFetchDetailsSuccess);

    Ok(Json(body))
}

pub async fn patch_update_task_by_id(
    Extension(database): Extension<DatabaseConnection>,
    Path(task_id): Path<i32>,
    Json(update_task_request_dto): Json<UpdateTaskRequestDto>,
) -> HandlerResult<Model> {
    let task: Option<Model> = Tasks::find_by_id(task_id).one(&database).await.unwrap();
    let mut task: tasks::ActiveModel = task.unwrap().into();

    if let Some(title) = update_task_request_dto.title {
        task.title = Set(title);
    }

    if let Some(description) = update_task_request_dto.description {
        task.description = Set(Some(description));
    }

    if let Some(priority) = update_task_request_dto.priority {
        task.priority = Set(Some(priority));
    }

    let task = task.update(&database).await;

    if let Err(_e) = &task {
        return Err(ErrorResponse::ResourceUpdateFailed);
    }

    let body = SuccessResponse::new(Some(task.unwrap()), ResourceUpdateSuccess);

    Ok(Json(body))
}

pub async fn put_update_task_by_id(
    Extension(database): Extension<DatabaseConnection>,
    Path(task_id): Path<i32>,
    Json(update_task_request_dto): Json<UpdateTaskRequestDto>,
) -> impl IntoResponse {
    let task: Option<Model> = Tasks::find_by_id(task_id).one(&database).await.unwrap();
    let mut task: tasks::ActiveModel = task.unwrap().into();

    task.title = Set(update_task_request_dto.title.unwrap());
    task.description = Set(update_task_request_dto.description);
    task.priority = Set(update_task_request_dto.priority);

    let task = task.update(&database).await;

    if let Err(_e) = &task {
        return Err(ErrorResponse::ResourceUpdateFailed);
    }

    let body = SuccessResponse::new(Some(task.unwrap()), ResourceUpdateSuccess);

    Ok(Json(body))
}

pub async fn delete_task_by_id(
    Extension(database): Extension<DatabaseConnection>,
    Path(task_id): Path<i32>,
) -> HandlerResult<DeleteResultDto> {
    let task = Tasks::find_by_id(task_id)
        .one(&database)
        .await
        .unwrap()
        .unwrap();
    let task = task.delete(&database).await;

    if let Err(_e) = &task {
        return Err(ErrorResponse::ResourceDeleteFailed);
    }

    let delete_result = DeleteResultDto {
        number_of_rows_affected: task.unwrap().rows_affected,
    };

    let body = SuccessResponse::new(Some(delete_result), ResourceDeleteSuccess);

    Ok(Json(body))
}
