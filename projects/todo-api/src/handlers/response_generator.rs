use crate::dtos::response_dtos::ErrorResponseDto;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::Json;
use serde::Serialize;
pub enum ErrorResponse {
    ResourceNotFound,
    ResourceCreateFailed,
    ResourceFetchListFailed,
    ResourceUpdateFailed,
    ResourceDeleteFailed,
    AuthenticationFailed,
    MissingAuthToken,
    InvalidAuthToken,
    UnhandledException(String),
    FailureToCreateResource(String),
}
impl IntoResponse for ErrorResponse {
    fn into_response(self) -> Response {
        let error_message: String = match &self {
            ErrorResponse::ResourceNotFound => "Resource not found, invalid identifier".to_owned(),
            ErrorResponse::ResourceCreateFailed => "Resource creation failed".to_owned(),
            ErrorResponse::ResourceFetchListFailed => "Resource list fetch failed".to_owned(),
            ErrorResponse::ResourceDeleteFailed => "Resource delete failed".to_owned(),
            ErrorResponse::ResourceUpdateFailed => "Resource update failed".to_owned(),
            ErrorResponse::AuthenticationFailed => "Invalid Email/Password".to_owned(),
            ErrorResponse::MissingAuthToken => "Authentication token is missing, attempting unauthorized request".to_owned(),
            ErrorResponse::InvalidAuthToken => "Invalid auth token, malicious token detected".to_owned(),
            ErrorResponse::UnhandledException(error_message) => error_message.clone(),
            ErrorResponse::FailureToCreateResource(error_message) => error_message.clone(),
        };

        let status_code: StatusCode = match &self {
            ErrorResponse::ResourceNotFound => StatusCode::NOT_FOUND,
            ErrorResponse::FailureToCreateResource(_) => StatusCode::CONFLICT,
            ErrorResponse::AuthenticationFailed => StatusCode::UNAUTHORIZED,
            ErrorResponse::MissingAuthToken => StatusCode::UNAUTHORIZED,
            ErrorResponse::InvalidAuthToken => StatusCode::UNAUTHORIZED,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        };

        let error_status_code: u32 = match &self {
            ErrorResponse::ResourceNotFound => 404,
            ErrorResponse::FailureToCreateResource(_) => 409,
            ErrorResponse::AuthenticationFailed  => 401,
            ErrorResponse::MissingAuthToken  => 401,
            ErrorResponse::InvalidAuthToken => 401,
            _ => 500,
        };

        (
            status_code,
            Json(ErrorResponseDto {
                error_message: error_message.to_owned(),
                error_status_code,
            }),
        )
            .into_response()
    }
}

#[allow(dead_code)]
pub enum ResponseMessage {
    ResourceCreateSuccess,
    ResourceFetchListSuccess,
    ResourceFetchDetailsSuccess,
    ResourceUpdateSuccess,
    ResourceDeleteSuccess,
    AuthenticationSuccess,
    CustomResponseMessage(String),
    Nothing,
}

#[derive(Serialize)]
pub struct SuccessResponse<T> {
    data: Option<T>,
    message: Option<String>,
}

impl<T> IntoResponse for SuccessResponse<T> {
    fn into_response(self) -> Response {
        (StatusCode::OK, self).into_response()
    }
}

impl<T> SuccessResponse<T> {
    pub fn new(data: Option<T>, message: ResponseMessage) -> Self {
        let message = match message {
            ResponseMessage::ResourceCreateSuccess => {
                Some("resource created successfully".to_owned())
            },
            ResponseMessage::ResourceFetchListSuccess => Some("resource list found".to_owned()),
            ResponseMessage::ResourceFetchDetailsSuccess => Some("resource details found".to_owned()),
            ResponseMessage::ResourceUpdateSuccess => Some("resource updated successfully".to_owned()),
            ResponseMessage::ResourceDeleteSuccess => Some("resource deleted successfully".to_owned()),
            ResponseMessage::AuthenticationSuccess => Some("authentication success".to_owned()),
            ResponseMessage::CustomResponseMessage(custom_message) => Some(custom_message),
            ResponseMessage::Nothing => None,
        };
        Self { data, message }
    }
}

pub type HandlerResult<T> = Result<Json<SuccessResponse<T>>, ErrorResponse>;
pub type MiddlewareRseult<T> = Result<T, ErrorResponse>;
