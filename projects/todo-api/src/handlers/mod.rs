use axum::Json;
use crate::{API_VERSION, ENVIRONMENT};
use crate::dtos::root_dtos::ApiDetailsDtO;
use crate::handlers::response_generator::{HandlerResult, ResponseMessage, SuccessResponse};

pub mod task_handler;
pub mod user_handler;
pub mod response_generator;
pub async fn ping_pong() -> HandlerResult<ApiDetailsDtO> {
    let body = SuccessResponse::new(
        Some(ApiDetailsDtO {
            version: API_VERSION.to_owned(),
            environment: ENVIRONMENT.to_owned()
        }),
        ResponseMessage::CustomResponseMessage("API is alive".to_owned())
    );

     Ok(Json(body))
}