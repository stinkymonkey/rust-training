use dotenvy::dotenv;
use dotenvy_macro::dotenv;
use todo_api::run_app;

#[tokio::main]
async fn main() {
    dotenv().ok();
    let database_uri = dotenv!("DATABASE_URL");
    run_app(database_uri).await;
}
