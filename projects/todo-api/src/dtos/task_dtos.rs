use serde::{Deserialize};

#[derive(Deserialize)]
pub struct CreateTaskRequestDto {
    pub title: String,
    pub priority: Option<String>,
    pub description: Option<String>
}

#[derive(Deserialize)]
pub struct UpdateTaskRequestDto {
    pub title: Option<String>,
    pub priority: Option<String>,
    pub description: Option<String>
}

#[derive(Deserialize)]
pub struct ListTasksRequestParametersDto {
    pub title: Option<String>,
    pub priority: Option<String>,
    pub description: Option<String>
}
