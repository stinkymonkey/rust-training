use serde::Serialize;

#[derive(Serialize)]
pub struct ErrorResponseDto {
    pub error_message: String,
    pub error_status_code: u32,
}