use serde::Serialize;

pub mod task_dtos;
pub mod user_dtos;
pub mod response_dtos;
pub mod root_dtos;

#[derive(Serialize)]
pub struct DeleteResultDto {
    pub number_of_rows_affected: u64
}