use serde::Serialize;

#[derive(Serialize)]
pub struct ApiDetailsDtO {
    pub version: String,
    pub environment: String,
}