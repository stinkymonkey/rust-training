use crate::{
    dtos::user_dtos::{TokenClaims, AuthUser},
    handlers::response_generator::{ErrorResponse, MiddlewareRseult},
    AUTH_TOKEN, JWT_SECRET, database::{prelude::Users, users},
};
use axum::{http::Request, middleware::Next, response::Response};
use jsonwebtoken::{decode, DecodingKey, Validation};
use sea_orm::{DatabaseConnection, EntityTrait, ColumnTrait, QueryFilter};
use axum::Extension;
use tower_cookies::Cookies;

pub async fn mw_require_auth<B>(
    cookies: Cookies,
    Extension(database): Extension<DatabaseConnection>,
    mut req: Request<B>,
    next: Next<B>,
) -> MiddlewareRseult<Response> {
    println!("req checkpoint - mw_require_auth");

    let token = cookies
        .get(AUTH_TOKEN)
        .map(|cookie| cookie.value().to_string())
        .or_else(|| None);
    let token = token.or_else(|| None);

    if let None = &token {
        return Err(ErrorResponse::MissingAuthToken);
    }

    let token = token.unwrap();

    let claims = decode::<TokenClaims>(
        &token,
        &DecodingKey::from_secret(JWT_SECRET.as_ref()),
        &Validation::default(),
    )
    .map_err(|_| ErrorResponse::InvalidAuthToken)?
    .claims;

    println!("token: {:?}", token);
    println!("claims: {:?}", claims.sub);

    let user_id: i32 = claims.sub.parse::<i32>().unwrap();

    let user = Users::find().filter(
        users::Column::Id.eq(user_id)
    ).one(&database)
    .await
    .map_err(|e| {
        ErrorResponse::UnhandledException(e.to_string())
    })?
    .ok_or_else(|| {
        ErrorResponse::InvalidAuthToken
    })?;

    let auth_user = AuthUser {
        user_id: user.id
    };

    req.extensions_mut().insert(auth_user);

    Ok(next.run(req).await)
}
