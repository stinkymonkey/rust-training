use crate::{handlers::{
    ping_pong,
    task_handler::{
        create_task, delete_task_by_id, get_all_tasks, get_task_by_id, patch_update_task_by_id,
        put_update_task_by_id,
    },
    user_handler::{create_user, login_user},
}, middlewares::mw_auth::mw_require_auth};
use axum::{routing::post, middleware};
use axum::{routing::get, Extension, Router};
use sea_orm::DatabaseConnection;
use tower_cookies::CookieManagerLayer;

const PATH_ROOT: &'static str = "/";
const PATH_TASKS: &'static str = "/tasks";
const PATH_TASKS_BY_ID: &'static str = "/tasks/:id";
const PATH_USERS: &'static str = "/users";
const PATH_USERS_LOGIN: &'static str = "/users/login";

pub fn create_routes(database: DatabaseConnection) -> Router {
    Router::new()
        .route(PATH_ROOT, get(ping_pong))
        .merge(task_routes())
        .merge(user_routes())
        .layer(CookieManagerLayer::new())
        .layer(Extension(database))
}

fn task_routes() -> Router {
    Router::new()
        .route(PATH_TASKS, post(create_task).get(get_all_tasks))
        .route(
            PATH_TASKS_BY_ID,
            get(get_task_by_id)
                .put(put_update_task_by_id)
                .patch(patch_update_task_by_id)
                .delete(delete_task_by_id)
        )
        .route_layer(middleware::from_fn(mw_require_auth))
}

fn user_routes() -> Router {
    Router::new()
        .route(PATH_USERS, post(create_user))
        .route_layer(middleware::from_fn(mw_require_auth))
        .route(PATH_USERS_LOGIN, post(login_user))
}
