use sea_orm::prelude::Expr;
use sea_orm::sea_query::{ColumnRef, SimpleExpr};
use sea_orm::sea_query::extension::postgres::PgExpr;

pub fn create_like_filter(column_ref: ColumnRef, query_string: String) -> SimpleExpr {
    Expr::ilike(Expr::col(column_ref), format!("%{}%", query_string))
}