mod handlers;
mod middlewares;
mod routes;

use routes::create_routes;

const SERVER_IP_PORT: &'static str = "0.0.0.0:3000";

pub async fn run_server() {
    let app = create_routes();

    axum::Server::bind(&SERVER_IP_PORT.parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}
