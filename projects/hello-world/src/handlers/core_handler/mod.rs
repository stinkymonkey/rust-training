use axum::{
    extract::{Path, Query},
    headers::UserAgent,
    http::{HeaderMap, StatusCode},
    response::{IntoResponse, Response},
    Extension, Json, TypedHeader,
};
use serde::{Deserialize, Serialize};

use crate::routes::SharedData;

#[derive(Serialize, Deserialize, Debug)]
pub struct MirrorJson {
    message: String,
}

#[derive(Deserialize, Serialize)]
pub struct MessageQuery {
    message: String,
    message_type: String,
}

#[derive(Clone)]
pub struct HeaderMessage(pub String);

pub async fn print_hello_world() -> String {
    "Hello World".to_owned()
}

pub async fn mirror_body_string(body: String) -> String {
    body
}

pub async fn mirror_body_json(Json(body): Json<MirrorJson>) -> Json<MirrorJson> {
    Json(body)
}

pub async fn get_things(Path(id): Path<u32>) -> String {
    id.to_string()
}

pub async fn get_query_parameters(Query(query): Query<MessageQuery>) -> Json<MessageQuery> {
    Json(query)
}

pub async fn mirror_user_agent(TypedHeader(user_agent): TypedHeader<UserAgent>) -> String {
    user_agent.to_string()
}

pub async fn mirror_custom_header(headers: HeaderMap) -> String {
    let message_value = headers.get("x-message").unwrap();
    let message = message_value.to_str();

    message.unwrap().to_string()
}

pub async fn middleware_message(Extension(shared_data): Extension<SharedData>) -> String {
    shared_data.message.to_owned()
}

pub async fn read_middleware_custom_header(Extension(message): Extension<HeaderMessage>) -> String {
    message.0
}

pub async fn always_errors() -> Result<(), StatusCode> {
    Err(StatusCode::INTERNAL_SERVER_ERROR)
}

pub async fn returns_201() -> Response {
    (StatusCode::CREATED, "Resource created".to_owned()).into_response()
}

#[derive(Serialize, Deserialize, Clone)]
pub struct RequestAuthLogin {
    username: String,
    password: String,
}

pub async fn get_json(Json(auth): Json<RequestAuthLogin>) -> Json<RequestAuthLogin> {
    Json(auth)
}

#[allow(unused_variables)]
pub async fn validate_data(Json(auth): Json<RequestAuthLogin>) {}
