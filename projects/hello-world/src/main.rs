use hello_world::run_server;

#[tokio::main]
async fn main() {
    run_server().await;
}
