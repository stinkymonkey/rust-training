use axum::{
    http::Method,
    middleware,
    routing::{get, post},
    Extension, Router,
};

use crate::handlers::core_handler;
use crate::middlewares::set_middleware_custom_header;
use tower_http::cors::{Any, CorsLayer};

const PATH_MIRROR_BODY_STRING: &'static str = "/mirror_body_string";
const PATH_MIRROR_BODY_JSON: &'static str = "/mirror_body_json";
const PATH_GET_THINGS: &'static str = "/get_things/:id";
const PATH_QUERY_STRING: &'static str = "/query_string";
const PATH_MIRROR_USER_AGENT: &'static str = "/mirror_user_agent";
const PATH_CUSTOM_HEADER: &'static str = "/mirror_custom_header";
const PATH_MIDDLEWARE_MESSAGE: &'static str = "/middleware_message";
const PATH_READ_MIDDLEWARE_CUSTOM_HEADER: &'static str = "/read_middleware_custom_header";
const PATH_ALWAYS_ERRORS: &'static str = "/always_errors";
const PATH_RETURNS_201: &'static str = "/returns_201";
const PATH_GET_JSON: &'static str = "/get_json";
const PATH_VALIDATE_DATA: &'static str = "/validate_data";

#[derive(Clone)]
pub struct SharedData {
    pub message: String,
}

pub fn create_routes() -> Router {
    let cors = CorsLayer::new()
        .allow_methods([Method::GET, Method::POST])
        .allow_origin(Any);

    let shared_data = SharedData {
        message: "This is a shared message".to_owned(),
    };

    Router::new()
        .route("/", get(core_handler::print_hello_world))
        .route(
            PATH_MIRROR_BODY_STRING,
            post(core_handler::mirror_body_string),
        )
        .route(PATH_MIRROR_BODY_JSON, post(core_handler::mirror_body_json))
        .route(PATH_GET_THINGS, get(core_handler::get_things))
        .route(PATH_QUERY_STRING, get(core_handler::get_query_parameters))
        .route(PATH_MIRROR_USER_AGENT, get(core_handler::mirror_user_agent))
        .route(PATH_CUSTOM_HEADER, get(core_handler::mirror_custom_header))
        .route(
            PATH_MIDDLEWARE_MESSAGE,
            get(core_handler::middleware_message),
        )
        .route(
            PATH_READ_MIDDLEWARE_CUSTOM_HEADER,
            get(core_handler::read_middleware_custom_header),
        )
        .layer(cors)
        .layer(Extension(shared_data))
        .route_layer(middleware::from_fn(set_middleware_custom_header))
        .route(PATH_ALWAYS_ERRORS, get(core_handler::always_errors))
        .route(PATH_RETURNS_201, post(core_handler::returns_201))
        .route(PATH_GET_JSON, post(core_handler::get_json))
        .route(PATH_VALIDATE_DATA, post(core_handler::validate_data))
}
